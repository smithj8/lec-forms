<?php
//PhD listing class
//j.smith8@lancaster.ac.uk

//start class
class phd_listing {

//constructor
function phd_listing(
$base="",
$directory="",
$show_division=1,
$show_category=1,
$show_year=1,
$show_index=1,
$show_user_link=1,
$show_funding_type_filter=1,
$show_study_type_filter=1,
$show_department_filter=1,
$show_text=1,
$heading="1",
$div_class="",
$list_class="",
$faculty_id="",
$department_id="",
$phd_id="",
$department_set="",
$funding_type_id="",
$funding_types="",
$study_type_id="",
$study_types="",
$departments="",
$title_text="Research Degree Opportunities",
$text="Currently available research degree opportunities:",
$no_results_text="No research degree opportunities are available at present",
$phd_title="",
$phd_listing_str="",
$funding_type_str="",
$study_type_str="",
$show_apply_online_link=1,
$apply_online_link=""

){
	global $base;

	//path variables
	$this->base=$base;
	$this->dir=$directory;

	//formatting variables
	$this->show_division=$show_division;
	$this->show_category=$show_category;
	$this->show_year=$show_year;
	$this->show_index=$show_index;
	$this->show_user_link=$show_user_link;
	$this->show_funding_type_filter=$show_funding_type_filter;
	$this->show_study_type_filter=$show_study_type_filter;
	$this->show_department_filter=$show_department_filter;
	$this->show_text=$show_text;
	$this->heading=$heading;
	$this->div_class=$div_class;
	$this->list_class=$list_class;
	$this->show_apply_online_link=$show_apply_online_link;

	//PhD variables
	$this->faculty_id = $faculty_id;
	$this->department_id = isset($_POST['department_id']) ? $_POST['department_id'] : $department_id;
	$this->department_set = $department_set;
	$this->funding_type_id = isset($_POST['funding_type_id']) ? $_POST['funding_type_id'] : $funding_type_id;
	$this->study_type_id = isset($_POST['study_type_id']) ? $_POST['study_type_id'] : $study_type_id;
	$this->phd_id = (isset($_GET['phd_id']) && is_numeric($_GET['phd_id'])) ? $_GET['phd_id'] : $phd_id;
	$this->funding_types = $funding_types;
	$this->study_types = $study_types;
	$this->departments = $departments;
	$this->title_text = $title_text;
	$this->text = $text;
	$this->no_results_text = $no_results_text;
	$this->phd_title = $phd_title;
	$this->phd_listing_str = $phd_listing_str;
	$this->funding_type_str = $funding_type_str;
	$this->study_type_str = $study_type_str;
}

//universal set function
function set($varname,$value) {
	$this->$varname=$value;
}

//return user string
function return_user($id) {

	//open variable
	$user_str = "";

	//SQL variable check
	if(is_numeric($id)){

		//get user info
		$user_query="SELECT t.title,u.first_name,u.middle_name,u.last_name,u.letters,u.email,u.website FROM users as u,titles as t WHERE u.title_id = t.title_id AND u.user_id = ".$id;
		$user_result=@mysql_query($user_query);

		//write user
		if($user_result && mysql_num_rows($user_result) > 0){
			while($row=mysql_fetch_array($user_result,MYSQL_NUM)){
				$user_str = $row[0]." ".$row[1]." ".$row[2]." ".$row[3]." ".$row[4];
				$user_str = trim(preg_replace("/ +/U"," ",$user_str));
				if($row[6] && $this->show_user_link==1){
					$user_str = "<a href=\"http://".$row[6]."\">".$user_str."</a>";
				}elseif($row[5] && $this->show_user_link==1){
					$user_str = "<a href=\"mailto:".$row[5]."\">".$user_str."</a>";
				}
			}
			mysql_free_result($user_result);
		}
	}
	return $user_str;
}

//return department string
function return_department($id) {

	//open variable
	$department_str = "";

	//SQL variable check
	if(is_numeric($id)){

		//get user info
		$department_query="SELECT department FROM departments WHERE department_id = ".$id;
		$department_result=@mysql_query($department_query);

		//write news
		if($department_result && mysql_num_rows($department_result) > 0){
			while($row=mysql_fetch_array($department_result,MYSQL_NUM)){
				$department_str = $row[0];
			}
			mysql_free_result($department_result);
		}
	}
	return $department_str;
}

//return funding type string
function return_funding_type($id) {

	//open variable
	$funding_type_str_str = "";

	//SQL variable check
	if(is_numeric($id)){

		//get user info
		$funding_type_str_query="SELECT funding_type FROM phd_funding_types WHERE funding_type_id = ".$id;
		$funding_type_str_result=@mysql_query($funding_type_str_query);

		//write funding types
		if($funding_type_str_result && mysql_num_rows($funding_type_str_result) > 0){
			while($row=mysql_fetch_array($funding_type_str_result,MYSQL_NUM)){
				$funding_type_str_str = $row[0];
			}
			mysql_free_result($funding_type_str_result);
		}
	}
	return $funding_type_str_str;
}

//return study type string
function return_study_type($id) {

	//open variable
	$study_type_str_str = "";

	//SQL variable check
	if(is_numeric($id)){

		//get user info
		$study_type_str_query="SELECT study_type FROM phd_study_types WHERE study_type_id = ".$id;
		$study_type_str_result=@mysql_query($study_type_str_query);

		//write study types
		if($study_type_str_result && mysql_num_rows($study_type_str_result) > 0){
			while($row=mysql_fetch_array($study_type_str_result,MYSQL_NUM)){
				$study_type_str_str = $row[0];
			}
			mysql_free_result($study_type_str_result);
		}
	}
	return $study_type_str_str;
}

//get funding_types
function get_funding_types() {

	//new array to hold funding type info
	$this->funding_types = array();

	//get funding types
	$funding_type_strs_query="SELECT funding_type_id,funding_type,funding_type_desc FROM phd_funding_types ORDER BY funding_type";
	$funding_type_strs_result=mysql_query($funding_type_strs_query);

	//write funding types
	if($funding_type_strs_result && mysql_num_rows($funding_type_strs_result) > 0){

		while($row=mysql_fetch_array($funding_type_strs_result,MYSQL_NUM)){
			$this->funding_types[$row[0]] = array('funding_type'=>$row[1],'funding_type_desc'=>$row[2]);
		}
		mysql_free_result($funding_type_strs_result);
	}
}

//get study_types
function get_study_types() {

	//new array to hold study type info
	$this->study_types = array();

	//get study types
	$study_type_strs_query="SELECT study_type_id,study_type,study_type_desc FROM phd_study_types ORDER BY study_type";
	$study_type_strs_result=mysql_query($study_type_strs_query);

	//write study types
	if($study_type_strs_result && mysql_num_rows($study_type_strs_result) > 0){

		while($row=mysql_fetch_array($study_type_strs_result,MYSQL_NUM)){
			$this->study_types[$row[0]] = array('study_type'=>$row[1],'study_type_desc'=>$row[2]);
		}
		mysql_free_result($study_type_strs_result);
	}
}

//get departments
function get_departments() {

	//new array to hold department info
	$this->departments = array();

	//SQL for faculty
	$departments_sql_str = is_numeric($this->faculty_id) ? " as d,faculties as f WHERE d.faculty_id = f.faculty_id" : "" ;

	//SQL for multiple departments
	if(preg_match("/^([0-9]+,)+$/U",$this->department_set)){

		//start sql string
		$departments_sql_str = " WHERE (";

		//trim off last comma and make into array
		$this->department_set = rtrim($this->department_set,",");
		$array = explode(",",$this->department_set);

		//loop through array and write into query
		foreach($array as $key=>$value){
			$departments_sql_str .= "department_id = '".$value."' OR ";
		}

		//tidy up query
		$departments_sql_str = rtrim($departments_sql_str," OR ").")";
	}

	//get departments
	$departments_query="SELECT department_id,department,department_website FROM departments".$departments_sql_str." ORDER BY department";
	$departments_result=mysql_query($departments_query);

	//write departments
	if($departments_result && mysql_num_rows($departments_result) > 0){

		while($row=mysql_fetch_array($departments_result,MYSQL_NUM)){
			$this->departments[$row[0]] = array('department'=>$row[1],'department_website'=>$row[2]);
		}
		mysql_free_result($departments_result);
	}
}

//write phd list
function write_phds() {

	//open variables
	$phds_list="";
	$title_block = "";
	$funding_type_strs_list="";
	$filter_options="";

	//get funding type and department arrays
	$this->get_funding_types();
	$this->get_study_types();
	$this->get_departments();

	//write funding type divs into str
	if(count($this->funding_types > 0)){

		//open list
		$funding_type_strs_list .= "<div class=\"hidden\">\n<h3>Funding Types</h3>";

		//write details
		foreach($this->funding_types as $key=>$value){
			$funding_type_strs_list .="<div id=\"funding_type_".$key."\"><h4>".$value['funding_type']."</h4>\n".format($value['funding_type_desc'])."</div>\n";
		}

		//close list
		$funding_type_strs_list .= "</div>\n";
	}

	//write study type divs into str
	if(count($this->study_types > 0)){

		$study_type_strs_list = "";

		//open list
		$study_type_strs_list .= "<div class=\"hidden\">\n<h3>Study Types</h3>";

		//write details
		foreach($this->study_types as $key=>$value){
			$study_type_strs_list .="<div id=\"study_type_".$key."\"><h4>".$value['study_type']."</h4>\n".format($value['study_type_desc'])."</div>\n";
		}

		//close list
		$study_type_strs_list .= "</div>\n";
	}

	//SQL and label for department
	$department_sql_str = is_numeric($this->department_id) ? "AND p.department_id = '".$this->department_id."' " : "" ;
	$department_str = is_numeric($this->department_id) ? $this->departments[$this->department_id]['department'] : "" ;

	//SQL for multiple departments
	if(preg_match("/^([0-9]+,)+$/U",$this->department_set)){

		//start sql string
		$department_sql_str .= "AND (";

		//trim off last comma and make into array
		$this->department_id = rtrim($this->department_id,",");
		$array = explode(",",$this->department_id);

		//loop through array and write into query
		foreach($array as $key=>$value){
			$department_sql_str .= "p.department_id = '".$value."' OR ";
		}

		//tidy up query
		$department_sql_str = rtrim($department_sql_str," OR ").") ";
	}

	//SQL and label for funding type
	$funding_type_str_sql_str = is_numeric($this->funding_type_id) ? "AND p.funding_type_id = '".$this->funding_type_id."' " : "" ;
	$funding_type_str_str = ($this->funding_type_id) ? $this->funding_types[$this->funding_type_id]['funding_type'] : "" ;

	//SQL and label for study type
	$study_type_str_sql_str = is_numeric($this->study_type_id) ? "AND p.study_type_id = '".$this->study_type_id."' " : "" ;
	$study_type_str_str = ($this->study_type_id) ? $this->study_types[$this->study_type_id]['study_type'] : "" ;

	//set page title
	$title_block .= $this->title_text;

	//add department or faculty name for title
	if($this->show_division==1 && $department_str){
		$title_block .= "<br/>\nin ".$department_str;
	}

	//heading tags round title
	$title_block = "<h".$this->heading.">".$title_block."</h".$this->heading.">\n";

	//filters form
	if($this->show_funding_type_filter==1 or $this->show_department_filter==1){
		$filter_options .= "<form class=\"frm-simple frm-ported\" id=\"phd_filters\" method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">\n";
		$filter_options .= "<fieldset>\n";
		//department
		if($this->show_department_filter==1){
			$filter_options .= "<label for=\"department_id\">Filter by department:</label>\n";
			$filter_options .= "<select id=\"department_id\" name=\"department_id\" onchange=\"this.form.submit();\">\n";
			$filter_options .= "<option value=\"\">Show opportunities in all departments...</option>\n";
			foreach($this->departments as $key=>$value){
				$filter_options .= "<option value=\"".$key."\"";
				if($this->department_id==$key){
					$filter_options .= " selected=\"selected\" ";
				}
				$filter_options .= ">".$value['department']."</option>\n";
			}
			$filter_options .= "</select>\n";
		}
		//funding type
		if($this->show_funding_type_filter==1){
			$filter_options .= "<label for=\"funding_type_id\">Filter by funding type:</label>\n";
			$filter_options .= "<select id=\"funding_type_id\" name=\"funding_type_id\" onchange=\"this.form.submit();\">\n";
			$filter_options .= "<option value=\"\">Show all funding types...</option>\n";
			foreach($this->funding_types as $key=>$value){
				$filter_options .= "<option value=\"".$key."\"";
				if($this->funding_type_id==$key){
					$filter_options .= " selected=\"selected\" ";
				}
				$filter_options .= ">".$value['funding_type']."</option>\n";
			}
			$filter_options .= "</select>\n";
		}
		//study type
		if($this->show_study_type_filter==1){
			$filter_options .= "<label for=\"study_type_id\">Filter by study type:</label>\n";
			$filter_options .= "<select id=\"study_type_id\" name=\"study_type_id\" onchange=\"this.form.submit();\">\n";
			$filter_options .= "<option value=\"\">Show all study types...</option>\n";
			foreach($this->study_types as $key=>$value){
				$filter_options .= "<option value=\"".$key."\"";
				if($this->study_type_id==$key){
					$filter_options .= " selected=\"selected\" ";
				}
				$filter_options .= ">".$value['study_type']."</option>\n";
			}
			$filter_options .= "</select>\n";
		}
		$filter_options .= "</fieldset>\n";
		$filter_options .= "</form>\n";
	}

	$title_block .= $filter_options;

	//add text below title
	if(	$this->show_text==1){
		$title_block .= "<h".($this->heading+1).">".$this->text."</h".($this->heading+1).">\n";
	}

	//add funding type text below title
	//if(	$funding_type_str_str){
	//	$title_block .= "<p><b>Funding Type:</b> <a href=\"#funding_type_".$this->funding_type_id."\" rel=\"facebox\">".$this->funding_types[$this->funding_type_id]['funding_type']."</a></p>\n";
	//}

	//add study type text below title
	//if(	$study_type_str_str){
	//	$title_block .= "<p><b>study Type:</b> <a href=\"#study_type_".$this->study_type_id."\" rel=\"facebox\">".$this->study_types[$this->study_type_id]['study_type']."</a></p>\n";
	//}

	//open div
	$phds_list .= "<div class=\"".$this->div_class."\">\n";

        // old query
	//$phds_query="SELECT phd_id,phd,supervisor1_id,supervisor2_id,DATE_FORMAT(deadline,'%a %e %M %Y'),ignore_deadline,department,department_website,p.funding_type_id,funding_type,funding_type_desc,p.study_type_id,study_type,study_type_desc,apply_online FROM phds as p,departments as d,phd_funding_types as ft,phd_study_types as st WHERE p.department_id = d.department_id AND p.funding_type_id = ft.funding_type_id AND p.study_type_id = st.study_type_id AND display_phd = '1' ".$department_sql_str.$funding_type_str_sql_str.$study_type_str_sql_str." AND (ignore_deadline = '1' OR DATE_SUB(CURDATE(),INTERVAL 0 DAY) <= deadline) ORDER BY phd";

        //get phds
        $phds_query="SELECT 
            phd_id,
            phd,
            supervisor1_id,
            supervisor2_id,
            DATE_FORMAT(deadline,'%a %e %M %Y'),
            ignore_deadline,
            department,
            department_website,
            p.funding_type_id,
            funding_type,
            funding_type_desc,
            p.study_type_id,
            study_type,
            study_type_desc,
            apply_online
            FROM phds as p,departments as d,phd_funding_types as ft,phd_study_types as st WHERE display_phd = '1' ".$funding_type_str_sql_str.$study_type_str_sql_str."AND p.department_id = '5242' AND p.funding_type_id = ft.funding_type_id AND p.study_type_id = st.study_type_id AND (ignore_deadline = '1' OR DATE_SUB(CURDATE(),INTERVAL 0 DAY) <= deadline) GROUP BY phd";
	$phds_result=mysql_query($phds_query);

	//write phds
	if($phds_result && mysql_num_rows($phds_result) > 0){

		//open list
		//$phds_list .= "<dl class=\"".$this->list_class."\">\n";

		while($row=mysql_fetch_array($phds_result,MYSQL_NUM)){

			//PhD project title
			$phds_list .= "<h3 id=\"phd_".$row[0]."\"><a href=\"".$_SERVER['PHP_SELF']."?phd_id=".$row[0]."\">".$row[1]."</a></h3>\n";

			$phds_list .= "<div class=\"pg-opportunity\">\n";
			$phds_list .= "<p>\n";

			//supervisors
                        $sFirst = $this->return_user($row[2]);
                        $sSecond = $this->return_user($row[3]);
			if(strlen($sFirst)){
				$supervisor_str = strlen($sSecond) ? "<strong>Supervisors:</strong> ".$sFirst." and ".$sSecond : "<strong>Supervisor:</strong> ".$sFirst;
				$this->phd_listing_str .= $supervisor_str."<br/>\n";
			}

			//department
			if($this->show_division == 1){
				$phds_list .= "<strong>Department:</strong> <a href=\"http://".$row[7]."\">".$row[6]."</a><br/>\n";
			}

			$phds_list .= "</p>\n";
			$phds_list .= "<p>\n";

			//deadline
			if($row[5] > 0){
				$phds_list .= "<strong>Deadline for Applications:</strong> Applications accepted all year round<br/>\n";
			}else{
				$phds_list .= "<strong>Deadline for Applications:</strong> ".$row[4]."<br/>\n";
			}

			//funding type
			//if(!$funding_type_str_str){
			/*(!empty($row[10])){
				$phds_list .= "<strong>Funding Type:</strong> <a href=\"#funding_type_".$row[8]."\" rel=\"facebox\">".$row[9]."</a><br/>\n";
			}else{*/
				$phds_list .= "<strong>Funding Type:</strong> ".$row[9]."<br/>\n";
			//}
			//}

			//study type
			//if(!$study_type_str_str){
			//	$phds_list .= "<strong>study Type:</strong> <a href=\"#study_type_".$row[11]."\" rel=\"facebox\">".$row[12]."</a><br/>\n";
				$phds_list .= "<strong>Type of Study:</strong> ".$row[12]."<br/>\n";
			//}

			$phds_list .= "</p>\n";

			if($this->show_apply_online_link==1 AND $row[14]==1){
				$phds_list .= "<div class=\"link\"><p><a href=\"../apply-online/?phd_id=".$row[0]."\">Apply online</a></p></div>\n";
			}

			$phds_list .= "</div>\n";
		}

		//close list
		//$phds_list .= "</dl>\n";

		mysql_free_result($phds_result);

	//no phds found
	}else{
                $phds_list .= "<p>".$this->no_results_text."</p>\n";
	}

	//close div
	$phds_list .= "</div>\n";

	//write all
	echo $title_block;
	echo $phds_list;
	echo $funding_type_strs_list;
}

//write details for a single phd
function get_phd() {

	//get phd
	$phd_query="SELECT

phd,
phd_desc,
funding_notes,
refs,
supervisor1_id,
supervisor2_id,
eprint_ids,
DATE_FORMAT(deadline,'%a %e %M %Y'),
ignore_deadline,
pdf,
display_pdf,
department,
department_website,
p.funding_type_id,
funding_type,
funding_type_desc,
p.study_type_id,
study_type,
study_type_desc,
apply_online

FROM phds as p,departments as d,phd_funding_types as ft,phd_study_types as st WHERE p.display_phd = '1' AND p.department_id = '5242' AND (ignore_deadline = '1' OR DATE_SUB(CURDATE(),INTERVAL 0 DAY) <= deadline) AND p.funding_type_id = ft.funding_type_id AND p.study_type_id = st.study_type_id AND p.phd_id = '".$this->phd_id."' ORDER BY p.phd LIMIT 1";

// old query
// SELECT is as above, FROM is as follows ...
//FROM phds as p,departments as d,phd_funding_types as ft,phd_study_types as st WHERE (ignore_deadline = '1' OR DATE_SUB(CURDATE(),INTERVAL 0 DAY) <= deadline) AND p.department_id = d.department_id AND p.funding_type_id = ft.funding_type_id AND p.study_type_id = st.study_type_id AND p.display_phd = '1' AND p.phd_id = '".$this->phd_id."' ORDER BY p.phd";

	$phd_result=mysql_query($phd_query);

	//write phds
	if($phd_result && mysql_num_rows($phd_result) > 0){

		while($row=mysql_fetch_array($phd_result,MYSQL_NUM)){

			//title
			$this->phd_title .= $row[0];
			$this->phd_listing_str .= "<h".$this->heading.">".$row[0]."</h".$this->heading.">\n";

			//open div
			$this->phd_listing_str .= "<div class=\"".$this->div_class."\">\n";

			$this->phd_listing_str .= "<p>\n";

			//supervisors
                        $sFirst = $this->return_user($row[4]);
                        $sSecond = $this->return_user($row[5]);
			if(strlen($sFirst)){
				$supervisor_str = strlen($sSecond) ? "<strong>Supervisors:</strong> ".$sFirst." and ".$sSecond : "<strong>Supervisor:</strong> ".$sFirst;
				$this->phd_listing_str .= $supervisor_str."<br/>\n";
			}

			//department
			if($this->show_division == 1){
				$this->phd_listing_str .= "<strong>Department:</strong> <a href=\"http://".$row[12]."\">".$row[11]."</a><br/>\n";
			}
			$this->phd_listing_str .= "</p>\n";
			$this->phd_listing_str .= "<p>\n";

			//deadline
			if($row[8] > 0){
				$this->phd_listing_str .= "<strong>Deadline for Applications:</strong> Applications accepted all year round<br/>\n";
			}else{
				$this->phd_listing_str .= "<strong>Deadline for Applications:</strong> ".$row[7]."<br/>\n";
			}

			//funding type
			/*if(!empty($row[10])){
				$this->phd_listing_str .= "<strong>Funding Type:</strong> <a href=\"#funding_type_".$row[13]."\" rel=\"facebox\">".$row[14]."</a><br/>\n";
			}else{*/
				$this->phd_listing_str .= "<strong>Funding Type:</strong> ".$row[14]."<br/>\n";
			//}

			//study type
			$this->phd_listing_str .= "<strong>Type of Study:</strong> ".$row[17]."<br/>\n";

			$this->phd_listing_str .= "</p>\n";

			if($this->show_apply_online_link==1 AND $row[19]==1){
				$this->phd_listing_str .= "<div class=\"link\"><p><a href=\"../apply-online/?phd_id=".$this->phd_id."\">Apply online</a></p></div>\n";
			}

			//description
			$this->phd_listing_str .= "<h".($this->heading+1).">Summary</h".($this->heading+1).">\n";
			$this->phd_listing_str .= format($row[1]);

			//funding notes
			if($row[2]){
				$this->phd_listing_str .= "<h".($this->heading+1).">Funding Notes</h".($this->heading+1).">\n";
				$this->phd_listing_str .= format($row[2]);
			}

			//eprints
			if($row[6]){
				//new parser instance
				$parser = new EprintsXMLParser;
				$parser->eprint_ids = $row[6];
				$parser->list_tag = "ol";
				$parser->body_tag = "li";
				$parser->print = 0;
				$parser->WriteEprints();
				if($parser->counter > 0){
					$this->phd_listing_str .= "<h".($this->heading+1).">References</h".($this->heading+1).">\n";
					$this->phd_listing_str .= $parser->eprints_html;
				}
			}

			//references
			if($row[3]){
				if(!isset($parser) OR (isset($parser->counter) AND $parser->counter == 0)){
					$this->phd_listing_str .= "<h".($this->heading+1).">References</h".($this->heading+1).">\n";
					$this->phd_listing_str .= format($row[3]);
				}
			}

			//notes (pdf)
			if($row[9] && ($row[10] > 0)){
				$this->phd_listing_str .= "<h".($this->heading+1).">More Information</h".($this->heading+1).">\n";
				$this->phd_listing_str .= "<ul class=\"link\">\n";
				$this->phd_listing_str .= "<li class=\"pdf\"><a href=\"http://www.lancs.ac.uk/sci-tech/downloads/".$row[9]."\" title=\"More information: ".$row[0]."\">Download more infomation</a></li>\n";
				$this->phd_listing_str .= "</ul>\n";
			}

			//close div
			$this->phd_listing_str .= "</div>\n";

			//hidden funding_type div
			$this->phd_listing_str .= "<div class=\"hidden\">\n<h".($this->heading+1).">Funding Type Details</h".($this->heading+1).">\n";

			//write details
			$this->phd_listing_str .= "<div id=\"funding_type_".$row[13]."\"><h".($this->heading+2).">".$row[14]."</h".($this->heading+2).">\n".format($row[15])."</div>\n";

			//close div
			$this->phd_listing_str .= "</div>\n";


		}
		mysql_free_result($phd_result);

	//no phd found
	}else{

			//title
			$this->phd_listing_str .= "<h".$this->heading.">Not Found</h".$this->heading.">\n";
			//open div
			$this->phd_listing_str .= "<div class=\"".$this->div_class."\">\n";
			$this->phd_listing_str .= "<p>Sorry, the deadline for this funding opportunity has expired.</p>\n";
			//close div
			$this->phd_listing_str .= "</div>\n";
	}
}

function write_phd(){
	$this->get_phd();
	echo $this->phd_listing_str;
	echo $this->funding_type_str;
}

//end class
}
?>