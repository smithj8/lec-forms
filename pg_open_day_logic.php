<?php
//$split=1;

include "fst/universal/pg_connect_rw.php";
include "fst/universal/mysql_connect_ro.php";
include "fst/universal/functions.php";

/* imported from pg_courses_array.php */

//define list of PG courses
$pg_courses_array = array(
'contamination-risk-assessment-and-remediation' => array('name' => 'MSc Contamination, Risk Assessment and Remediation', 'link' => 'contamination-risk-assessment-and-remediation', 'display' => true),
'data-science-for-the-environment' => array('name' => 'MSc Data Science for the Environment', 'link' => 'data-science-for-the-environment', 'display' => true),
'ecology-and-conservation' => array('name' => 'MSc Ecology and Conservation', 'link' => 'ecology-and-conservation', 'display' => true),
'energy-and-the-environment' => array('name' => 'MSc Energy and the Environment ', 'link' => 'energy-and-the-environment', 'display' => true),
'environmental-and-biochemical-toxicology' => array('name' => 'MSc Environmental and Biochemical Toxicology', 'link' => 'environmental-and-biochemical-toxicology', 'display' => true),
'environment-and-development' => array('name' => 'MA/MSc Environment and Development', 'link' => 'environment-and-development', 'display' => true),
'environmental-management-and-consultancy' => array('name' => 'MA Environmental Management and Consultancy', 'link' => 'environmental-management-and-consultancy', 'display' => true),
'environmental-science-and-technology' => array('name' => 'MSc Environmental Science and Technology', 'link' => 'environmental-science-and-technology', 'display' => true),
'international-innovation-environmental-science' => array('name' => 'MSc International Innovation (Environmental Science)', 'link' => 'international-innovation-environmental-science', 'display' => true),
'resource-and-environmental-management' => array('name' => 'MSc Resource and Environmental Management', 'link' => 'resource-and-environmental-management', 'display' => true),
'sustainable-agriculture-and-food-security' => array('name' => 'MSc Sustainable Agriculture and Food Security ', 'link' => 'sustainable-agriculture-and-food-security', 'display' => true),
'sustainable-water-management' => array('name' => 'MSc Sustainable Water Management', 'link' => 'sustainable-water-management', 'display' => true),
'volcanology-and-geological-hazards' => array('name' => 'MSc Volcanology and Geological Hazards', 'link' => 'volcanology-and-geological-hazards', 'display' => true),
);

//advertisers array here
$advertisers=array(
    "advertiser_01" => "Action Network Magazine",
    "advertiser_02" => "Agent",
    "advertiser_03" => "British Council",
    "advertiser_04" => "Education UK",
    "advertiser_05" => "Environmentalist",
    "advertiser_06" => "Facebook",
    "advertiser_07" => "Find A Masters",
    "advertiser_08" => "Google",
    "advertiser_09" => "iGraduate",
    "advertiser_10" => "Independent Website",
    "advertiser_11" => "Independent/i Newspaper",
    "advertiser_12" => "LEC Email",
    "advertiser_13" => "LEC Newsletter (email)",
    "advertiser_14" => "LEC Website",
    "advertiser_15" => "Masters Compare",
    "advertiser_16" => "Postgraduate Fair",
    "advertiser_17" => "Postgraduate Studentships",
    "advertiser_18" => "Prospects",
    "advertiser_19" => "Twitter",
);


//titles array here
$titles=array(
    "Mr",
    "Mrs",
    "Miss",
    "Ms",
    "Dr",
);

//countries array here
$countries=array(
    "Afghanistan",
    "Aland Islands",
    "Albania",
    "Algeria",
    "American Samoa",
    "Andorra",
    "Angola",
    "Anguilla",
    "Antarctica",
    "Antigua and Barbuda",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahamas",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bermuda",
    "Bhutan",
    "Bolivia, Plurinational State of",
    "Bonaire, Sint Eustatius and Saba",
    "Bosnia and Herzegovina",
    "Botswana",
    "Bouvet Island",
    "Brazil",
    "British Indian Ocean Territory",
    "Brunei Darussalam",
    "Bulgaria",
    "Burkina Faso",
    "Burundi",
    "Cambodia",
    "Cameroon",
    "Canada",
    "Cape Verde",
    "Cayman Islands",
    "Central African Republic",
    "Chad",
    "Chile",
    "China",
    "Christmas Island",
    "Cocos (Keeling) Islands",
    "Colombia",
    "Comoros",
    "Congo",
    "Congo, The Democratic Republic of the",
    "Cook Islands",
    "Costa Rica",
    "Cote d\'Ivoire",
    "Croatia",
    "Cuba",
    "Curacao",
    "Cyprus",
    "Czech Republic",
    "Denmark",
    "Djibouti",
    "Dominica",
    "Dominican Republic",
    "Ecuador",
    "Egypt",
    "El Salvador",
    "Equatorial Guinea",
    "Eritrea",
    "Estonia",
    "Ethiopia",
    "Falkland Islands (Malvinas)",
    "Faroe Islands",
    "Fiji",
    "Finland",
    "France",
    "French Guiana",
    "French Polynesia",
    "French Southern Territories",
    "Gabon",
    "Gambia",
    "Georgia",
    "Germany",
    "Ghana",
    "Gibraltar",
    "Greece",
    "Greenland",
    "Grenada",
    "Guadeloupe",
    "Guam",
    "Guatemala",
    "Guernsey",
    "Guinea",
    "Guinea-Bissau",
    "Guyana",
    "Haiti",
    "Heard Island and McDonald Islands",
    "Holy See (Vatican City State)",
    "Honduras",
    "Hong Kong",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran, Islamic Republic of",
    "Iraq",
    "Ireland",
    "Isle of Man",
    "Israel",
    "Italy",
    "Jamaica",
    "Japan",
    "Jersey",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kiribati",
    "Korea, Democratic People\'s Republic of",
    "Korea, Republic of",
    "Kuwait",
    "Kyrgyzstan",
    "Lao People\'s Democratic Republic",
    "Latvia",
    "Lebanon",
    "Lesotho",
    "Liberia",
    "Libyan Arab Jamahiriya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macao",
    "Macedonia, The Former Yugoslav Republic of",
    "Madagascar",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Marshall Islands",
    "Martinique",
    "Mauritania",
    "Mauritius",
    "Mayotte",
    "Mexico",
    "Micronesia, Federated States of",
    "Moldova, Republic of",
    "Monaco",
    "Mongolia",
    "Montenegro",
    "Montserrat",
    "Morocco",
    "Mozambique",
    "Myanmar",
    "Namibia",
    "Nauru",
    "Nepal",
    "Netherlands",
    "New Caledonia",
    "New Zealand",
    "Nicaragua",
    "Niger",
    "Nigeria",
    "Niue",
    "Norfolk Island",
    "Northern Mariana Islands",
    "Norway",
    "Occupied Palestinian Territory",
    "Oman",
    "Pakistan",
    "Palau",
    "Panama",
    "Papua New Guinea",
    "Paraguay",
    "Peru",
    "Philippines",
    "Pitcairn",
    "Poland",
    "Portugal",
    "Puerto Rico",
    "Qatar",
    "Reunion",
    "Romania",
    "Russian Federation",
    "Rwanda",
    "Saint Barthelemy",
    "Saint Helena, Ascension and Tristan da Cunha",
    "Saint Kitts and Nevis",
    "Saint Lucia",
    "Saint Martin (French part)",
    "Saint Pierre and Miquelon",
    "Saint Vincent and The Grenadines",
    "Samoa",
    "San Marino",
    "Sao Tome and Principe",
    "Saudi Arabia",
    "Senegal",
    "Serbia",
    "Seychelles",
    "Sierra Leone",
    "Singapore",
    "Sint Maarten (Dutch part)",
    "Slovakia",
    "Slovenia",
    "Solomon Islands",
    "Somalia",
    "South Africa",
    "South Georgia and the South Sandwich Islands",
    "South Sudan",
    "Spain",
    "Sri Lanka",
    "Sudan",
    "Suriname",
    "Svalbard and Jan Mayen",
    "Swaziland",
    "Sweden",
    "Switzerland",
    "Syrian Arab Republic",
    "Taiwan, Province of China",
    "Tajikistan",
    "Tanzania, United Republic of",
    "Thailand",
    "Timor-Leste",
    "Togo",
    "Tokelau",
    "Tonga",
    "Trinidad and Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Turks and Caicos Islands",
    "Tuvalu",
    "Uganda",
    "Ukraine",
    "United Arab Emirates",
    "United Kingdom",
    "United States",
    "United States Minor Outlying Islands",
    "Uruguay",
    "Uzbekistan",
    "Vanuatu",
    "Venezuela, Bolivarian Republic of",
    "Viet Nam",
    "Virgin Islands, British",
    "Virgin Islands, U.S.",
    "Wallis and Futuna",
    "Western Sahara",
    "Yemen",
    "Zambia",
    "Zimbabwe",
);

/** open days **/

//get open days array from xml
$xml_str = file_get_contents('http://www.lancaster.ac.uk/fas/fst-legacy/xml/event_list.xml?site=14&type=7&range=future');
$xml = simplexml_load_string($xml_str);
$open_days = array();
foreach ($xml as $key => $value) {
    //exclude rogue open days
    if ($value->event_id != 451) {
        $id = $value->event_id;
        $date = $value->dates->date_string;
        if (preg_match("/virtual/Ui", $value->title)) {
            $date .= " (Virtual Open Day)";
        }
        settype($id, "integer");
        settype($date, "string");
        $open_days["open_day_".$id] = $date;
    }
}
//$open_days = array_reverse($open_days);

/** form handling **/

//set variables

$errors = "";
$conf = "";

//clear all values
function clear_booking_form()
{
    global $title,
    $first_name,
    $last_name,
    $email,
    $address1,
    $address2,
    $town,
    $postcode,
    $country,
    $phone,
    $first_degree,
    $first_degree_at,
    $where_advertised_other,
    $interested_dates,
    $interested_courses,
    $where_advertised,
    $interested_courses_list,
    $interested_dates_list,
    $extra_visitors,
    $where_advertised_list;

    $title="";
    $first_name="";
    $last_name="";
    $email="";
    $address1="";
    $address2="";
    $town="";
    $postcode="";
    $country="";
    $phone="";
    $first_degree="";
    $first_degree_at="";
    $where_advertised_other="";
    $interested_dates="";
    $interested_courses="";
    $where_advertised="";
    $interested_courses_list = array();
    $interested_dates_list = array();
    $extra_visitors=0;
    $where_advertised_list = array();
}

//empty arrays
$interested_courses_list = array();
$interested_dates_list = array();
$where_advertised_list = array();

//handle dates checkboxes
foreach ($open_days as $key => $value) {
    if (isset($_POST[$key]) && $_POST[$key]==$key) {
        $interested_dates_list[$key] = $value;
    }
}

//handle courses checkboxes
foreach ($pg_courses_array as $value) {
    if (isset($_POST[$value['link']]) && $_POST[$value['link']]==$value['link']) {
        $interested_courses_list[$value['link']] = $value['name'];
    }
}

//handle where_advertised checkboxes
foreach ($advertisers as $key => $value) {
    if (isset($_POST[$key]) && $_POST[$key]==$key) {
        $where_advertised_list[$key] = $value;
    }
}

//print_r($where_advertised_list);

//post variables
$title = isset($_POST['title']) ? $_POST['title'] : "";
$first_name = isset($_POST['first_name']) ? $_POST['first_name'] : "";
$last_name = isset($_POST['last_name']) ? $_POST['last_name'] : "";
$email = isset($_POST['email']) ? $_POST['email'] : "";
$address1 = isset($_POST['address1']) ? $_POST['address1'] : "";
$address2 = isset($_POST['address2']) ? $_POST['address2'] : "";
$town = isset($_POST['town']) ? $_POST['town'] : "";
$postcode = isset($_POST['postcode']) ? $_POST['postcode'] : "";
$country = isset($_POST['country']) ? $_POST['country'] : "";
$phone = isset($_POST['phone']) ? $_POST['phone'] : "";
$first_degree = isset($_POST['first_degree']) ? $_POST['first_degree'] : "";
$first_degree_at = isset($_POST['first_degree_at']) ? $_POST['first_degree_at'] : "";
$where_advertised_other = isset($_POST['where_advertised_other']) ? $_POST['where_advertised_other'] : "";
$interested_dates = isset($_POST['interested_dates']) ? $_POST['interested_dates'] : "";
$extra_visitors = isset($_POST['extra_visitors']) ? $_POST['extra_visitors'] : 0;
$interested_courses = isset($_POST['interested_courses']) ? $_POST['interested_courses'] : "";
$where_advertised = isset($_POST['where_advertised']) ? $_POST['where_advertised'] : "";

$extra_visitors=intval($extra_visitors);

//submit conditional
//--------------------------------------

if (isset($_POST['submitted'])) {

    //id
    //title

    //first_name
    if ($first_name) {
        if (preg_match("/^[[:alpha:].'-]{1,15}$/", stripslashes(trim($first_name)))) {
            $first_name=ucwords($first_name);
        } else {
            $errors.="<p>Please check your first name</p>";
        }
    } else {
        $errors.="<p>Please supply a first name</p>";
    }

    //last_name
    if ($last_name) {
        if (preg_match("/^[[:alpha:].'-]{2,30}$/", stripslashes(trim($last_name)))) {
            $last_name=ucwords($last_name);
        } else {
            $errors.="<p>Please check your last name</p>";
        }
    } else {
        $errors.="<p>Please supply a last name</p>";
    }

    //email
    if ($email) {
        if (preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/", stripslashes(trim($email)))) {
            $email=strtolower($email);
        } else {
            $errors.="<p>Please check your email address</p>";
        }
    } else {
        $errors.="<p>Please supply an email address</p>";
    }

    //address1
    //if (!$address1) {
    //    $errors.="<p>Please fill in your address</p>";
    //}

    //town
    //if (!$town) {
    //    $errors.="<p>Please fill in your town or city</p>";
    //}

    //postcode
    //if (!$postcode) {
    //    $errors.="<p>Please fill in your postal code</p>";
    //}

    //check phone number
    if ($phone) {
        if (!preg_match("/^[0-9 ]+$/", stripslashes(trim($phone)))) {
            $errors.="<p>Please check your phone number</p>";
        }
    }

    //first_degree
    //first_degree_at

    //interested_dates
    $interested_dates = "";
    if (count($interested_dates_list) > 0) {
        foreach ($interested_dates_list as $key => $value) {
            $interested_dates .= $value." ";
        }
    } else {
        $errors.="<p>Please tell us which dates you're interested in</p>";
    }

    //interested_courses
    $interested_courses = "";
    if (count($interested_courses_list) > 0) {
        foreach ($interested_courses_list as $key => $value) {
            $interested_courses .= $value." ";
        }
    } else {
        $interested_courses .= "No preferred courses";
    }

    //where_advertised
    $where_advertised = "";
    if (count($where_advertised_list) > 0) {
        foreach ($where_advertised_list as $key => $value) {
            $where_advertised .= $value." ";
        }
    } else {
        $where_advertised .= "No selections";
    }

    //where_advertised_other
    //date

    //if everything's OK
    if (!$errors) {

        //check email is not duplicated
        //$query="SELECT id FROM lec_pg_bookings WHERE email='$email'";
        //$result=@pg_query($query);

        //email is not duplicated
        //if (pg_num_rows($result)==0) {

        //escape text fields
        $first_name=ucwords(escape_data($first_name));
        $last_name=ucwords(escape_data($last_name));
        $email=strtolower(escape_data($email));
        $address1=ucwords(escape_data($address1));
        $address2=ucwords(escape_data($address2));
        $town=ucwords(escape_data($town));
        $postcode=strtoupper(escape_data($postcode));
        $phone=escape_data($phone);
        $first_degree=escape_data($first_degree);
        $first_degree_at=ucwords(escape_data($first_degree_at));
        $where_advertised_other=escape_data($where_advertised_other);

        //put in DB
        $query="INSERT INTO lec_pg_bookings (
        title,
        first_name,
        last_name,
        email,
        address1,
        address2,
        town,
        country,
        postcode,
        phone,
        first_degree,
        first_degree_at,
        where_advertised_other,
        interested_dates,
        extra_visitors,
        interested_courses,
        where_advertised,
        date
        ) VALUES (
        '$title',
        '$first_name',
        '$last_name',
        '$email',
        '$address1',
        '$address2',
        '$town',
        '$country',
        '$postcode',
        '$phone',
        '$first_degree',
        '$first_degree_at',
        '$where_advertised_other',
        '$interested_dates',
        '$extra_visitors',
        '$interested_courses',
        '$where_advertised',
        'now'
        )";

        $result=pg_query($query);    //run the query

        //if it ran OK
        if ($result) {

            //get ref
            $query="SELECT id FROM lec_pg_bookings WHERE email='$email' ORDER BY id DESC";
            $result=@pg_query($query);
            $ref=@pg_fetch_result($result, 0, 0);
            pg_free_result($result);
            $conf.="<p>Thank you for taking an interest in Masters programmes at the Lancaster Environment Centre your reference number for the event is <b>".$ref."</b></p>";

            //send an email here
            $message="Further information about the open day will be sent to you around two weeks before the event.\n\nPlease email the LEC PG office via lec.pg@lancaster.ac.uk if you have any queries.\n\nYou supplied the following details:\n\n";
            //name and address
            $name="";
            $name.=trim($title." ".$first_name." ".$last_name)."\n";
            $name.=$address1."\n";
            if ($address2) {
                $name.=$address2."\n";
            }
            $name.=$town."\n";
            $name.=$postcode."\n";
            $name.=$country."\n\n";
            if ($phone) {
                $name.="Phone: ".$phone."\n\n";
            }
            //other details
            $f_degree="";
            if ($first_degree) {
                $f_degree.="First Degree: ".$first_degree."\n";
            }
            if ($first_degree_at) {
                $f_degree.="First Degree from: ".$first_degree_at."\n\n";
            }
            //dates interest in
            $int_dates="";
            $int_dates.="Interested in these dates:\n\n";
            foreach ($interested_dates_list as $key => $value) {
                $int_dates.="\t".$value."\n";
            }
            $int_dates.="\nExtra visitors: ".$extra_visitors."\n";
            //courses interest in
            $int_courses="";
            $int_courses.="\nInterested in these courses:\n\n";
            foreach ($interested_courses_list as $key => $value) {
                $int_courses.="\t".$value."\n";
            }
            $body1=stripslashes($message.$name.$f_degree.$int_dates.$int_courses);
            $body2=stripslashes($name.$f_degree.$int_dates.$int_courses);
            $headers = "From: ".$email.PHP_EOL;
            //$headers .= "BCC: James Smith <j.smith8@lancaster.ac.uk>".PHP_EOL;
            mail($email, 'LEC Masters Open Day', $body1, 'From:lec.pg@lancaster.ac.uk');    //email to client
            mail('lec.pg@lancaster.ac.uk', 'LEC PG Open Day booking #'.$ref, $body2, $headers);//email to office


            //clear all variables
            clear_booking_form();

        //if it did not run OK
        } else {

            //error log message if required
            $errors.="<p>Sorry - Your booking request has not been completed due to a system error</p>";
        }

    }
}
/** end form handling **/
?>
