<?php
$split=1;

//titles array
$titles = [
"",
"Miss",
"Mr",
"Mrs",
"Ms",
"Dr",
];
asort($titles);

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

//include "fst/lec/header_dev.php";
require_once("fst/universal/fst_website_rw.php");
//----------------------------------------------------------

/** form handling **/

class application
{

    private $application_id = null;
    public $title = null;
    public $first_name = null;
    public $last_name = null;
    public $email = null;
    public $phd_id = null;
    public $cv = null;
    public $letter = null;
    public $errors = null;
    public $stage = 0;
    public $conf = null;
    private $department_id = 5242;
    private $emailed = 0;
    public $phds = array();
    private $file_path = "/export/depts/iens/lec/postgraduate/pgresearch/admin/docs/";

    public function __construct()
    {

        //retrieve session
        $this->application_id = isset($_SESSION['application_id']) ? $_SESSION['application_id'] : null ;

        if (isset($_GET['new-application']) && $_GET['new-application'] == 1) {
            $this->application_id = null;
            session_destroy();
        }

        //get phd opportunities
        $this->getPhds();

        //get saved application data
        $this->getData();

        //id from query string
        if (!$this->phd_id and isset($_GET['phd_id']) and is_int($_GET['phd_id'])) {
            $this->phd_id = $_GET['phd_id'];
        }

        //check form info
        if ($_POST) {

            if ($this->stage == 0) {

                $this->title = trim(stripslashes(@$_POST['title']));
                $this->first_name = trim(stripslashes(@$_POST['first_name']));
                $this->last_name = trim(stripslashes(@$_POST['last_name']));
                $this->email = trim(strtolower(stripslashes(@$_POST['email'])));
                $this->phd_id = trim(strtolower(stripslashes(@$_POST['phd_id'])));

                //first_name
                if ($this->first_name) {
                    if (!preg_match("/^[[:alpha:].'-]{1,15}$/", $this->first_name)) {
                        $this->errors.="<p>Please check your first name</p>";
                    }
                } else {
                    $this->errors.="<p>Please supply a first name</p>";
                }

                //last_name
                if ($this->last_name) {
                    if (!preg_match("/^[[:alpha:].'-]{2,30}$/", $this->last_name)) {
                        $this->errors.="<p>Please check your last name</p>";
                    }
                } else {
                    $this->errors.="<p>Please supply a last name</p>";
                }

                //email
                if ($this->email) {
                    if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/", $this->email)) {
                        $this->errors.="<p>Please check your email address</p>";
                    }
                } else {
                    $this->errors.="<p>Please supply an email address</p>";
                }

                //project
                if (!$this->phd_id) {
                    $this->errors.="<p>Please choose a PhD project</p>";
                }

                if (!$this->errors) {
                    $this->stage++;
                    $this->conf = "<p>Part one of three complete</p>";
                }

            } elseif ($this->stage == 1) {

                //cv upload
                $this->cv = $this->checkFile("cv");

                if (!$this->errors) {
                    $this->stage++;
                    $this->conf = "<p>Part two of three complete</p>";
                }

            } elseif ($this->stage == 2) {

                //letter upload
                $this->letter = $this->checkFile("letter");

                if (!$this->errors) {
                    $this->stage++;
                    $this->conf = "<p>Part three of three complete</p>";
                    $this->confEmail();
                }
            } elseif ($this->stage == 3) {

                $this->conf = "<p>Part three of three complete</p>";

            }

            if (!$this->errors) {
                $this->saveData();
            }
        }
    }

    private function confEmail()
    {

        //to
        $recipient = $this->first_name." ".$this->last_name." <".$this->email.">";

        //subject
        $subject = "LEC PhD Application (ref #".$this->application_id.")";

        //message
        $body = "Dear ".$this->first_name."\r\n";
        $body .= "\r\n";
        $body .= "\r\n";
        $body .= "Thank you for your PhD or MSc-R application. If you are selected for interview we will contact you after the closing date.\r\n";
        $body .= "\r\n";
        $body .= "Please note all applicants will be contacted regarding the outcome of their application after the studentships have been appointed to.\r\n";
        $body .= "\r\n";
        $body .= "In the meantime if you have any questions please contact me via lec.pg@lancaster.ac.uk.\r\n";
        $body .= "\r\n";
        $body .= "Best wishes\r\n";
        $body .= "\r\n";
        $body .= "Andy Harrod\r\n";
        $body .= "Postgraduate Research (PGR) Co-ordinator\r\n";
        $body .= "Lancaster Environment Centre\r\n";
        $body .= "\r\n";

        //headers
        $header = "Reply-To: LEC Postgraduate Office <lec.pg@lancaster.ac.uk>\r\n";
        $header .= "From: LEC Postgraduate Office <lec.pg@lancaster.ac.uk>\r\n";
        //$header .= "BCC: James Smith <j.smith8@lancaster.ac.uk>\r\n";
        $header .= "Content-Type: text/plain;\r\n";

        if ($this->emailed == 0) {
            if (mail($recipient, $subject, $body, $header)) {
                $this->emailed = 1;
            }
        }
    }

    private function checkFile($file)
    {

        $filename = null;
        $file_ok = false;

        if (isset($_FILES[$file]) and is_uploaded_file($_FILES[$file]['tmp_name'])) {

            $file_ok = true;

            //mime type
            $finfo = new finfo;
            $mimetype = $finfo->file($_FILES[$file]['tmp_name'], FILEINFO_MIME);

            //wrong file type
            if (!preg_match("@^application/(pdf|msword|vnd.openxmlformats-officedocument.wordprocessingml.document)@",$mimetype)) {
                $this->errors .= "<p>Uploaded file must be a PDF or MS Word document</p>";
                unlink($_FILES[$file]['tmp_name']);
                $file_ok = false;
            }

            //file too big
            if ($_FILES[$file]['size'] > 1000000) { //over 1000k
                $this->errors .= "<p>Uploaded file must be under 1MB</p>";
                unlink($_FILES[$file]['tmp_name']);
                $file_ok = false;
            }

            //upload
            if ($file_ok==true) {

                //file extension
                switch($_FILES[$file]['type']) {

                    case "application/pdf":
                        $ext = ".pdf";
                        break;

                    case "image/pdf":
                        $ext = ".pdf";
                        break;

                    case "application/msword":
                        $ext = ".doc";
                        break;

                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                        $ext = ".docx";
                        break;

                    case "application/vnd.ms-word.document.12":
                        $ext = ".docx";
                        break;

                    default:
                        $ext = ".txt";
                }
                $filename = $this->application_id."-".$file.$ext;
                copy($_FILES[$file]['tmp_name'], $this->file_path.$filename);
                unlink($_FILES[$file]['tmp_name']);
                $this->conf.="<p>The ".$file." was uploaded successfully</p>";
            }

        } else {
            $this->errors .= "<p>Please select a file for upload</p>";
        }
        return $filename;
    }

    private function getData()
    {
        global $fst_website;
        if ($this->application_id) {
            $sql = "SELECT title, first_name, last_name, email, phd_id, cv, letter, stage, emailed FROM phd_applications WHERE application_id = ?";
            try {
                $q = $fst_website->prepare($sql);
                $q->execute(array($this->application_id));
            } catch (PDOException $e) {
                $this->errors.="<p>System error: ".$e->getMessage()."</p>";
            }
            $result = $q->fetch();
            $this->title = $result['title'];
            $this->first_name = $result['first_name'];
            $this->last_name = $result['last_name'];
            $this->email = $result['email'];
            $this->phd_id = $result['phd_id'];
            $this->cv = $result['cv'];
            $this->letter = $result['letter'];
            $this->stage = $result['stage'];
            $this->emailed = $result['emailed'];
        }
    }

    private function getPhds()
    {
        global $fst_website;
        $sql = "SELECT phd_id, phd, phd_desc FROM phds WHERE department_id = ? and apply_online = '1' and display_phd = '1' and (ignore_deadline = '1' OR DATE_SUB(CURDATE(), INTERVAL 0 DAY) <= deadline) ORDER BY phd";
        try {
            $q = $fst_website->prepare($sql);
            $q->execute(array($this->department_id));
        } catch (PDOException $e) {
            $this->errors.="<p>System error: ".$e->getMessage()."</p>";
        }
        $result = $q->fetchAll();
        $this->phds = $result;
    }

    private function saveData()
    {
        global $fst_website;
        $datetime = new DateTime(null, new DateTimeZone('Europe/London'));
        $timestamp = $datetime->format('Y-m-d H:i:s');

        //new application
        if (!$this->application_id) {

            $sql = "INSERT INTO phd_applications (title, first_name, last_name, email, phd_id, cv, letter, timestamp, stage, emailed) VALUES (:title, :first_name, :last_name, :email, :phd_id, :cv, :letter, :timestamp, :stage, :emailed)";
            try {
                $q = $fst_website->prepare($sql);
                $values = array(":title"=>$this->title, ":first_name"=>$this->first_name, ":last_name"=>$this->last_name, ":email"=>$this->email, ":phd_id"=>$this->phd_id, ":cv"=>$this->cv, ":letter"=>$this->letter, ":timestamp"=>$timestamp, ":stage"=>$this->stage, ":emailed"=>$this->emailed);
                $q->execute($values);
            } catch (PDOException $e) {
                $this->errors.="<p>System error: ".$e->getMessage()."</p>";
            }
            $_SESSION['application_id'] = $fst_website->lastInsertId();

        //ongoing application
        } else {

            $sql = "UPDATE phd_applications SET title=?, first_name=?, last_name=?, email=?, phd_id=?, cv=?, letter=?, timestamp=?, stage=?, emailed=? WHERE application_id=?";
            try {
                $q = $fst_website->prepare($sql);
                $q->execute(array($this->title, $this->first_name, $this->last_name, $this->email, $this->phd_id, $this->cv, $this->letter, $timestamp, $this->stage, $this->emailed, $this->application_id));
            } catch (PDOException $e) {
                $this->errors.="<p>System error: ".$e->getMessage()."</p>";
            }
        }
    }

//end class
}

//new class instance
$application = new application;
?>

<h1>LEC Postgraduate Research Applications</h1>
<?php
//display error messages
if (isset($application->errors) && strlen($application->errors)) {
    echo "<div class=\"frm-ported-errors\">".$application->errors."</div>\n\n";
}

//display confirmation messages
/*if (isset($application->conf) && strlen($application->conf)) {
    echo "<div class=\"frm-ported-conf\">".$application->conf."</div>\n\n";
}*/

if ($application->stage == 0) {

    //there are some PhDs
    if ($application->phds) {
        ?>

        <form method="post" id="phd_apply" action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" class="frm-ported frm-simple">
        <fieldset>
        
        <ol>
        <li>
        <label for="title">Title</label>
        </li>
        
        <li>
        <select class="styled" name="title" id="title">
        <?php
        foreach ($titles as $value) {
            echo "<option value=\"".$value."\"";
                if ($application->title==$value) {
                    echo " selected=\"selected\"";
                }
            echo ">".$value."</option>\n";
        }
        ?>
        </select>
        </li>

        <li>
        <label for="first_name"><b>First Name</b><span class="required">*</span></label>
        </li>
        
        <li>
        <input class="styled" type="text" name="first_name" id="first_name" <?php echo "value=\"".$application->first_name."\"" ?>/>
        </li>

        <li>
        <label for="last_name"><b>Last Name</b><span class="required">*</span></label>
        </li>
        
        <li>
        <input class="styled" type="text" name="last_name" id="last_name" <?php echo "value=\"".$application->last_name."\"" ?>/>
        </li>

        <li>
        <label for="email"><b>Your email</b><span class="required">*</span></label>
        </li>

        <li>
        <input class="styled" type="text" name="email" id="email" <?php echo "value=\"".$application->email."\"" ?>/>
        </li>

        <?php
        if ($application->phds) {
            echo "<li>\n";
            echo "<label for=\"phd_id\"><b>Project</b><span class=\"required\">*</span></label></li><li>\n";
            echo "<select class=\"styled\" name=\"phd_id\" id=\"phd_id\">\n";
            echo "<option value=\"\"></option>\n";
            foreach ($application->phds as $value) {
                echo "<option title=\"".$value['phd']."\" value=\"".$value['phd_id']."\"";
                        if ($application->phd_id==$value['phd_id']) echo " selected=\"selected\"";
                echo ">".substr($value['phd'], 0, 55)."...</option>\n";
            }
            echo "</select>\n";
            echo "</li>\n";
        }
        ?>
        
        <li><input type="image" value="send" class="button" src="<t4 type="media" id="22727" formatter="path/*" />"/></li>

        </ol>

        </fieldset>
        </form>

    <?php
    //nothing to apply for
    } else {
        echo "<div class=\"errors\"><p>There are no PhD opportunities that you can apply online for at the moment</p></div>\n";
    }

} elseif ($application->stage == 1) {
?>

    <form method="post" id="phd_apply" enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" class="frm-ported frm-simple">
    <fieldset>
    <ol>
    <li><label for="cv"><strong>Please upload your CV</strong>:<span class="required">*</span></label></li>
    <li><input type="file" name="cv" id="cv"/></li>
    <li><input type="image" value="send" class="button" src="<t4 type="media" id="22727" formatter="path/*" />"/></li>
    </ol>
    </fieldset>
    </form>

<?php
} elseif ($application->stage == 2) {
?>

    <form method="post" id="phd_apply" enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>" class="frm-ported frm-simple">
    <fieldset>
    <ol>
    <li><label for="cv"><strong>Please upload your cover letter</strong>:</label></li>
    <li><input type="file" name="letter" id="letter"/></li>
    <li><input type="image" value="send" class="button" src="<t4 type="media" id="22727" formatter="path/*" />"/></li>
    </ol>
    </fieldset>
    </form>

<?php
} elseif ($application->stage == 3) {
    
    //this is the confirmation screen
    ?>
    <div class="frm-ported-conf">
    <p>Thanks for your application; You will receive a confirmation email.</p>
    <p><a href="../">Back to LEC Research Degree Opportunities</a></p>
    <p><a href="?new-application=1">Start a new application</a></p>
    </div>

    <?php
}

//----------------------------------------------------------
include "fst/universal/jquery_forms.php";
//include "fst/lec/footer_dev.php";
?>